# KnowledgeHub - Domain Coverage

This is collection of relevant questions and corresponding SPARQL-Queries, that answer those questions. The questions are grouped according to the different entities of interest (datasets, organizations, ...). The entities appear in alphabetical order. The first query is useful to get an overview of all entities available in the Knowledge Hub. The questions listed below form, altogether, the domain coverage of the Knowledge Hub. For details, see the [NFDI4Earth Deliverable D4.3.2](https://zenodo.org/records/7950860).

***Overview of the types of entities***
| ID    | Question | Query/ies |
|---|---|---|
| TY001	| What are the types of entities available in the knowledge graph? | [TY001](queries/TY001.rq)|

&nbsp;
&nbsp;

### Aggregator

| ID    | Question | Query/ies |
|---|---|---|
| AG001	| What are all entities of type Aggregator? | [AG001](queries/AG001.rq)|
| AG001_2	| What are name and geometry of Aggregator? | [AG001_2](queries/AG001_2.rq)|
| AG002_1	| What are all attributes available for the type "Aggregator"? | [AG002_1](queries/AG002_1.rq)|
| AG002_2	| How many attributes are available for the type "Aggregator"? | [AG002_2](queries/AG002_2.rq)|

### Article

| ID    | Question | Query/ies |
|---|---|---|
| AT001	| What are all entities of type schema:Article? | [AT001](queries/AT001.rq)|
| AT002_1	| What are all attributes available for the type "schema:Article"? | [AT002_1](queries/AT002_1.rq)|
| AT002_2	| How many attributes are available for the type "schema:Article"? | [AT002_2](queries/AT002_2.rq)|

### Dataset

| ID    | Question | Query/ies |
|---|---|---|
| DA001	| What are all entities of type dcat:Dataset? | [DA001](queries/DA001.rq)|
| DA002_1	| What are all attributes available for the type "dcat:Dataset"? | [DA002_1](queries/DA002_1.rq)|
| DA002_2	| How many attributes are available for the type "dcat:Dataset"? | [DA002_2](queries/DA002_2.rq)|
| DA003_1	| What are the datasets having the string 'world settlement footprint' in title or description? | [DA003_1](queries/DA003_1.rq)|

### LHBArticle

| ID    | Question | Query/ies |
|---|---|---|
| LH001	| What are all entities of type LHBArticle? | [LH001](queries/LH001.rq)|
| LH002_1	| What are all attributes available for the type "LHBArticle"? | [LH002_1](queries/LH002_1.rq)|
| LH002_2	| How many attributes are available for the type "LHBArticle"? | [LH002_2](queries/LH002_2.rq)|

### LearningResource

| ID    | Question | Query/ies |
|---|---|---|
| LR001	| What are all entities of type LearningResource? | [LR001](queries/LR001.rq)|
| LR002_1	| What are all attributes available for the type "LearningResource"? | [LR002_1](queries/LR002_1.rq)|
| LR002_2	| How many attributes are available for the type "LearningResource"? | [LR002_2](queries/LR002_2.rq)|

### MetadataStandard

| ID    | Question | Query/ies |
|---|---|---|
| MS001	| What are all entities of type MetadataStandard? | [MS001](queries/MS001.rq)|
| MS002_1	| What are all attributes available for the type "MetadataStandard"? | [MS002_1](queries/MS002_1.rq)|
| MS002_2	| How many attributes are available for the type "MetadataStandard"? | [MS002_2](queries/MS002_2.rq)|

### Organization

| ID    | Question | Query/ies |
|---|---|---|
| OG001	| What are all entities of type Organization? | [OG001](queries/OG001.rq)|
| OG002_1	| What are all attributes available for the type "Organization"? | [OG002_1](queries/OG002_1.rq)|
| OG002_2	| How many attributes are available for the type "Organization"? | [OG002_2](queries/OG002_2.rq)|

### Person

| ID    | Question | Query/ies |
|---|---|---|
| PE001	| What are all entities of type Person? | [PE001](queries/PE001.rq)|
| PE002_1	| What are all attributes available for the type "Person"? | [PE002_1](queries/PE002_1.rq)|
| PE002_2	| How many attributes are available for the type "Person"? | [PE002_2](queries/PE002_2.rq)|

### Registry

| ID    | Question | Query/ies |
|---|---|---|
| REG001	| What are all entities of type Registry? | [REG001](queries/REG001.rq)|
| REG002_1	| What are all attributes available for the type "Registry"? | [REG002_1](queries/REG002_1.rq)|
| REG002_2	| How many attributes are available for the type "Registry"? | [REG002_2](queries/REG002_2.rq)|

### Repository

| ID    | Question | Query/ies |
|---|---|---|
| REP001	| What are all entities of type Repository? | [REP001](queries/REP001.rq)|
| REP002_1	| What are all attributes available for the type "Repository"? | [REP002_1](queries/REP002_1.rq)|
| REP002_2	| How many attributes are available for the type "Repository"? | [REP002_2](queries/REP002_2.rq)|

### ResearchProject

| ID    | Question | Query/ies |
|---|---|---|
| RP001	| What are all entities of type ResearchProject? | [RP001](queries/RP001.rq)|
| RP002_1	| What are all attributes available for the type "ResearchProject"? | [RP002_1](queries/RP002_1.rq)|
| RP002_2	| How many attributes are available for the type "ResearchProject"? | [RP002_2](queries/RP002_2.rq)|


### SoftwareSourceCode

| ID    | Question | Query/ies |
|---|---|---|
| SC001	| What are all entities of type SoftwareSourceCode? | [SC001](queries/SC001.rq)|
| SC002_1	| What are all attributes available for the type "SoftwareSourceCode"? | [SC002_1](queries/SC002_1.rq)|
| SC002_2	| How many attributes are available for the type "SoftwareSourceCode"? | [SC002_2](queries/SC002_2.rq)|


<!--- Template for a new table (including first line)

### EntityType

| ID    | Question | Query/ies |
|---|---|---|
| XX001	| What are all entities of type EntityType? | [XX001](queries/XX001.rq)|

-->


<!---


### Organizations

| ID    | Question | Query/ies |
|---|---|---|
| OR001	| What is the URL of the homepage for the organization with the following name: 'Karlsruhe Institute of Technology'? | [OR001_1](queries/OR001_1.rq),[OR001_2](queries/OR001_2.rq) |
| OR002 | What is the URL of the homepage for the organization with the following ID: 'https://nfdi4earth-knowledgehub.geo.tu-dresden.de/api/objects/n4ekh/a38143be5e15bed94a20' | [OR003_1](queries/OR003_1.rq) |
| OR003 | Which organizations have not defined any homepage? | [OR003_1](queries/OR003_1.rq) |
| OR004 | Which services are published by the organization? | [OR004_1](queries/OR004_1.rq) |
| OR005 | What is the geolocation of the organization called 'TU Dresden'? | [OR005_1](queries/OR005_1.rq) |
| OR006 | What is the geolocation of all organizations, that are members of the NFDI4Earth consortium? | [OR006_1](queries/OR006_1.rq) |

### Repositories

| ID | Question | Query/ies |
|----|----------|-----------|
| DR1 | At which repository can I archive my [geophysical] data of [2] GB?| [OR004_1](queries/OR004_1.rq) |
| DR2 | What is the temporal coverage of a data repository?||
| DR3 | What is the spatial coverage of a data repository?||
| DR4 | What is the curation policy of the data repository?||
| DR5 | Which licences are supported by the data repository?||
| DR6 | Does the repository give identifiers for its ressources?||
| DR7 | Which metadata harversting interface is supported by the repository?||
| DR8 | Which type of (persistent) identifiers are used by the repository?||
| DR9 | What is the thematic area/subject of a repository?||
| DR10 | Limitations of data deposit at the repository?||
| DR11 | When was the medatada for a given repository first collected/last updated?||
| DR12 | Is the repository still available?||
| DR13 | Which repository allows long term archiving?||

-->

# Notes

This question-based approach takes inspiration from the [GeoSPARQLBenchmark](https://github.com/OpenLinkSoftware/GeoSPARQLBenchmark).

It is directly linked to the [Knowledge Hub landing page project](https://git.rwth-aachen.de/nfdi4earth/knowledgehub/kh_landingpage) as all the questions and examples are taken to explain the basic idea and demonstrate usage of the [Knowledge Hub](https://knowledgehub.nfdi4earth.de).
